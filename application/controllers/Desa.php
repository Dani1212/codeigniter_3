<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Desa extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('DesaModel');
	}

	public function index()
	{
		$data['desa'] = $this->DesaModel->view();
		$this->load->view('desa/index', $data);
	}

	public function create()
	{
		$this->load->view('desa/create');
	}

	public function edit($id)
	{
		$data['desa'] = $this->DesaModel->show($id)->row();
		$this->load->view('desa/edit', $data);
	}

	public function simpan()
	{
		$this->DesaModel->save();
		return redirect('/desa');
	}

	public function ubah($id)
	{
		$this->DesaModel->edit($id);
		return redirect('/desa');
	}

	public function hapus($id)
	{
		$this->DesaModel->delete($id);
		return redirect('/desa');
	}
}
