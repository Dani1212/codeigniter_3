<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Rab extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('RabModel');
	}

	public function index()
	{
		$data['rab'] = $this->RabModel->view();
		$this->load->view('rab/index', $data);
	}

	public function create()
	{
		$this->load->view('rab/create');
	}

	public function edit($id)
	{
		$data['rab'] = $this->RabModel->show($id)->row();
		$this->load->view('rab/edit', $data);
	}

	public function simpan()
	{
		$this->RabModel->save();
		return redirect('/rab');
	}

	public function ubah($id)
	{
		$this->RabModel->edit($id);
		return redirect('/rab');
	}

	public function hapus($id)
	{
		$this->RabModel->delete($id);
		return redirect('/rab');
	}
}

