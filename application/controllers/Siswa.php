<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Siswa extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('SiswaModel');
	}

	public function index()
	{
		$data['siswa'] = $this->SiswaModel->view();
		$this->load->view('siswa/index', $data);
	}

	public function simpan()
	{
		if($this->SiswaModel->validation("save")){
			$this->SiswaModel->save();

			$html = $this->load->view('siswa/view', array('siswa' => $this->SiswaModel->view()), true);
			$callback = array(
				'status' => 'sukses',
				'pesan' => 'Data berhasil disimpan!',
				'html' => $html
			);
		}else{
			$callback = array(
				'status' => 'gagal',
				'pesan' => validation_errors()
			);
		}

		echo json_encode($callback);
	}

	public function ubah($id)
	{
		if($this->SiswaModel->validation("update")){
			$this->SiswaModel->edit($id);

			$html = $this->load->view('siswa/view', array('siswa' => $this->SiswaModel->view()), true);
			$callback = array(
				'status' => 'sukses',
				'pesan' => 'Data berhasil diubah!',
				'html' => $html
			);
		}else{
			$callback = array(
				'status' => 'gagal',
				'pesan' => validation_errors()
			);
		}

		echo json_encode($callback);
	}

	public function hapus($id)
	{
		$this->SiswaModel->delete($id);
		$html = $this->load->view('siswa/view', array('siswa' => $this->SiswaModel->view()), true);
		$callback = array(
			'status' => 'sukses',
			'pesan' => 'Data berhasil dihapus!',
			'html' => $html
		);

		echo json_encode($callback);
	}

}


