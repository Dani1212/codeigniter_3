<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Posyandu extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();;
		$this->load->model('PosyanduModel');
	}

	public function index()
	{
		$data['posyandu'] = $this->PosyanduModel->view();
		$this->load->view('posyandu/index', $data);
	}
	
	public function create()
	{
		$this->load->view('posyandu/create');
	}

	public function edit($id)
	{
		$data['posyandu'] = $this->PosyanduModel->show($id)->row();
		$this->load->view('posyandu/edit', $data);
	}

	public function simpan()
	{
		$this->PosyanduModel->save();
		return redirect('/posyandu');
	}

	public function ubah($id)
	{
		$this->PosyanduModel->edit($id);
		return redirect('/posyandu');
	}

	public function hapus($id)
	{
		$this->PosyanduModel->delete($id);
		return redirect('/posyandu');
	}
}

