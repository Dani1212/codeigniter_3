<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Master extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('MasterModel');
	}

	public function index()
	{
		$data['rw'] = $this->MasterModel->rw();
		$data['rt'] = $this->MasterModel->rt();
		$data['karangTaruna'] = $this->MasterModel->karangTaruna();
		$data['posyandu'] = $this->MasterModel->posyandu();
		$data['rab'] = $this->MasterModel->rab();
		$data['desa'] = $this->MasterModel->desa();
		$this->load->view('master/index', $data);
	}
}
