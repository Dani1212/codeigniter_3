<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class KarangTaruna extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();;
		$this->load->model('KarangTarunaModel');
	}

	public function index()
	{
		$data['karangtaruna'] = $this->KarangTarunaModel->view();
		$this->load->view('karangtaruna/index', $data);
	}
	
	public function create()
	{
		$this->load->view('karangtaruna/create');
	}

	public function edit($id)
	{
		$data['karangtaruna'] = $this->KarangTarunaModel->show($id)->row();
		$this->load->view('karangtaruna/edit', $data);
	}

	public function simpan()
	{
		$this->KarangTarunaModel->save();
		return redirect('/karangtaruna');
	}

	public function ubah($id)
	{
		$this->KarangTarunaModel->edit($id);
		return redirect('/karangtaruna');
	}

	public function hapus($id)
	{
		$this->KarangTarunaModel->delete($id);
		return redirect('/karangtaruna');
	}
}

