<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Rt extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();;
		$this->load->model('RtModel');
	}

	public function index()
	{
		$data['rt'] = $this->RtModel->view();
		$this->load->view('rt/index', $data);
	}
	
	public function create()
	{
		$this->load->view('rt/create');
	}

	public function edit($id)
	{
		$data['rt'] = $this->RtModel->show($id)->row();
		$this->load->view('rt/edit', $data);
	}

	public function simpan()
	{
		$this->RtModel->save();
		return redirect('/rt');
	}

	public function ubah($id)
	{
		$this->RtModel->edit($id);
		return redirect('/rt');
	}

	public function hapus($id)
	{
		$this->RtModel->delete($id);
		return redirect('/rt');
	}
}

