<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Jurusan extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('JurusanModel');
	}

	public function index()
	{
		$data['jurusan'] = $this->JurusanModel->view();
		$this->load->view('jurusan/index', $data);
	}

	public function create()
	{
		$this->load->view('jurusan/create');
	}

	public function edit($id)
	{
		$data['jurusan'] = $this->JurusanModel->show($id)->row();
		$this->load->view('jurusan/edit', $data);
	}

	public function simpan()
	{
		if($this->JurusanModel->validation("save")){
			$this->JurusanModel->save();
			return redirect('/jurusan');
		}else{
			$this->load->view('jurusan/create', array('listErrors' => validation_errors()));
		}
	}

	public function ubah($id)
	{
		if($this->JurusanModel->validation("update")){
			$this->JurusanModel->edit($id);

			return redirect('/jurusan');
		}else{
			$this->load->view('jurusan/edit/'.$id, array('listErrors' => validation_errors()));
		}
	}

	public function hapus($id)
	{
		$this->JurusanModel->delete($id);
		return redirect('/jurusan');
	}
}
