<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Rw extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();;
		$this->load->model('RwModel');
	}

	public function index()
	{
		$data['rw'] = $this->RwModel->view();
		$data['rt'] = $this->RwModel->rt();
		$data['karangTaruna'] = $this->RwModel->karangTaruna();
		$data['posyandu'] = $this->RwModel->posyandu();
		$this->load->view('rw/index', $data);
	}
	
	public function create()
	{
		$this->load->view('rw/create');
	}

	public function edit($id)
	{
		$data['rw'] = $this->RwModel->show($id)->row();
		$this->load->view('rw/edit', $data);
	}

	public function simpan()
	{
		$this->RwModel->save();
		return redirect('/rw');
	}

	public function ubah($id)
	{
		$this->RwModel->edit($id);
		return redirect('/rw');
	}

	public function hapus($id)
	{
		$this->RwModel->delete($id);
		return redirect('/rw');
	}
}

