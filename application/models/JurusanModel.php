<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class JurusanModel extends CI_Model {

	public function view()
	{
		return $this->db->get('jurusan')->result();
	}

	public function show($id)
	{
		$this->db->select('*');
     	$this->db->from('jurusan');
     	$this->db->where('id', $id);

     	return $this->db->get();
	}

	public function validation()
	{
		$this->load->library('form_validation');
	
		$this->form_validation->set_rules('nama_jurusan', 'Nama Jurusan', 'required|max_length[50]', array('required' => 'Masukan Nama Jurusan!'));
		$this->form_validation->set_rules('deskripsi', 'Deskripsi Jurusan', 'required');
	
		$this->form_validation->set_rules('nama_jurusan', 'Nama Jurusan', 'required|max_length[50]');
		$this->form_validation->set_rules('deskripsi', 'Deskripsi Jurusan', 'required');
		

		if($this->form_validation->run()){
			return true;
		}else {
			return false;
		}
	}

	public function save()
	{
		$data = array(
			"nama_jurusan" => $this->input->post('nama_jurusan'),
			"deskripsi" => $this->input->post('deskripsi')
		);

		$this->db->insert('jurusan', $data);
	}

	public function edit($id)
	{
		$data = array(
			"nama_jurusan" => $this->input->post('nama_jurusan'),
			"deskripsi" => $this->input->post('deskripsi')
		);

		$this->db->where('id', $id);
		$this->db->update('jurusan', $data);
	}

	public function delete($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('jurusan');
	}

}
