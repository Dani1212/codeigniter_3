<?php 

if(!defined('BASEPATH')) exit('No direct script access allowed');

class PosyanduModel extends CI_Model {
	public function view()
	{
		return $this->db->get('posyandu')->result();
	}

	public function show($id)
	{
		$this->db->select('*');
		$this->db->from('posyandu');
		$this->db->where('id', $id);

		return $this->db->get();
	}

	public function validation()
	{
		$this->load->library('form_validation');

		$this->form_validation->set_rules('nama_posyandu', 'Nama posyandu', 'required');
		$this->form_validation->set_rules('ketua_posyandu', 'Ketua Posyandu', 'required');
		$this->form_validation->set_rules('wakil_posyandu', 'Wakil Ketua', 'required');
		$this->form_validation->set_rules('sekretaris', 'Sekretaris', 'required');
		$this->form_validation->set_rules('bendahara', 'Bendahara', 'required');
	}

	public function save()
	{
		$data = array(
			"nama_posyandu" => $this->input->post('nama_posyandu'),
			"ketua_posyandu" => $this->input->post('ketua_posyandu'),
			"wakil_posyandu" => $this->input->post('wakil_posyandu'),
			"sekretaris" => $this->input->post('sekretaris'),
			"bendahara" => $this->input->post('bendahara'),
		);
		$this->db->insert('posyandu', $data);
	}

	public function edit($id)
	{
		$data = array(
			"nama_posyandu" => $this->input->post('nama_posyandu'),
			"ketua_posyandu" => $this->input->post('ketua_posyandu'),
			"wakil_posyandu" => $this->input->post('wakil_posyandu'),
			"sekretaris" => $this->input->post('sekretaris'),
			"bendahara" => $this->input->post('bendahara'),
		);
		
		$this->db->where('id', $id);
		$this->db->update('posyandu', $data);
	}

	public function delete($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('posyandu');
	}
}
