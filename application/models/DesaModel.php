<?php 

if(!defined('BASEPATH')) exit('No direct script access allowed');

class DesaModel extends CI_Model {

	public function view()
	{
		return $this->db->get('data_desa')->result();
	}

	public function show($id)
	{
		$this->db->select('*');
		$this->db->from('data_desa');
		$this->db->where('id', $id);

		return $this->db->get();
	}

	public function validation($mode)
	{
		$this->load->library('form_validation');
	
		$this->form_validation->set_rules('nama', 'Nama', 'required');
		$this->form_validation->set_rules('tempat_lahir', 'Tempat Lahir', 'required');
		$this->form_validation->set_rules('tanggal_lahir', 'Tanggal Lahir', 'required');
		$this->form_validation->set_rules('jenis_kelamin', 'Jenis Kelamin', 'required');
		$this->form_validation->set_rules('alamat', 'Alamat', 'required');
		$this->form_validation->set_rules('pendidikan_terakhir', 'Pendidikan Terakhir', 'required');
		$this->form_validation->set_rules('jabatan', 'Jabatan', 'required');
		$this->form_validation->set_rules('nomor_sk', 'Nomor SK', 'required');
		$this->form_validation->set_rules('tanggal_sk', 'Tanggal SK', 'required');
		$this->form_validation->set_rules('tanggal_pelantikan', 'Tanggal Pelantikan', 'required');
		$this->form_validation->set_rules('keterangan', 'Keterangan', 'required');
		
	}

	public function save()
	{
		$data = array(
			"nama" => $this->input->post('nama'),
			"tempat_lahir" => $this->input->post('tempat_lahir'),
			"tanggal_lahir" => $this->input->post('tanggal_lahir'),
			"jenis_kelamin" => $this->input->post('jenis_kelamin'),
			"alamat" => $this->input->post('alamat'),
			"pendidikan_terakhir" => $this->input->post('pendidikan_terakhir'),
			"jabatan" => $this->input->post('jabatan'),
			"nomor_sk" => $this->input->post('nomor_sk'),
			"tanggal_sk" => $this->input->post('tanggal_sk'),
			"tanggal_pelantikan" => $this->input->post('tanggal_pelantikan'),
			"keterangan" => $this->input->post('keterangan')
		);

		$this->db->insert('data_desa', $data);
	}

	public function edit($id)
	{
		$data = array(
			"nama" => $this->input->post('nama'),
			"tempat_lahir" => $this->input->post('tempat_lahir'),
			"tanggal_lahir" => $this->input->post('tanggal_lahir'),
			"jenis_kelamin" => $this->input->post('jenis_kelamin'),
			"alamat" => $this->input->post('alamat'),
			"pendidikan_terakhir" => $this->input->post('pendidikan_terakhir'),
			"jabatan" => $this->input->post('jabatan'),
			"nomor_sk" => $this->input->post('nomor_sk'),
			"tanggal_sk" => $this->input->post('tanggal_sk'),
			"tanggal_pelantikan" => $this->input->post('tanggal_pelantikan'),
			"keterangan" => $this->input->post('keterangan')
		);

		$this->db->where('id', $id);
		$this->db->update('data_desa', $data);
	}

	public function delete($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('data_desa');
	}

}


