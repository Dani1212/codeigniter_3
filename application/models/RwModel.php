<?php 

if(!defined('BASEPATH')) exit('No direct script access allowed');

class RwModel extends CI_Model {
	public function view()
	{
		return $this->db->get('rw')->result();
	}

	public function show($id)
	{
		$this->db->select('*');
		$this->db->from('rw');
		$this->db->where('id', $id);

		return $this->db->get();
	}

	public function rt()
	{
		return $this->db->get('rt')->result();
	}

	public function karangTaruna()
	{
		return $this->db->get('karang_taruna')->result();
	}

	public function posyandu()
	{
		return $this->db->get('posyandu')->result();
	}

	public function validation()
	{
		$this->load->library('form_validation');

		$this->form_validation->set_rules('nama_rw', 'Nama RW', 'required');
		$this->form_validation->set_rules('nama_wilayah', 'Nama Wilayah', 'required');
		$this->form_validation->set_rules('ketua_rw', 'Ketua RW', 'required');
		$this->form_validation->set_rules('penasihat', 'Penasihat', 'required');
		$this->form_validation->set_rules('sekretaris', 'Sekretaris', 'required');
		$this->form_validation->set_rules('bendahara', 'Bendahara', 'required');
		$this->form_validation->set_rules('seksi_keamanan', 'Seksi Keamanan', 'required');
        $this->form_validation->set_rules('seksi_keagamanan', 'Seksi Keagamanan', 'required');
        $this->form_validation->set_rules('seksi_pemuda', 'Seksi Pemuda', 'required');
        $this->form_validation->set_rules('seksi_humas', 'Seksi Humas', 'required');
        $this->form_validation->set_rules('seksi_pembangunan_dan_kebersihan', 'Seksi Pembangunan Dan Kebersihan', 'required');
        $this->form_validation->set_rules('seksi_pemberdayaan_perempuan', 'Sumber Pemberdayaan Perempuan', 'required');
	}

	public function save()
	{
		$data = array(
			"nama_rw" => $this->input->post('nama_rw'),
			"nama_wilayah" => $this->input->post('nama_wilayah'),
			"ketua_rw" => $this->input->post('ketua_rw'),
			"penasihat" => $this->input->post('penasihat'),
			"sekretaris" => $this->input->post('sekretaris'),
			"bendahara" => $this->input->post('bendahara'),
			"seksi_keamanan" => $this->input->post('seksi_keamanan'),
            "seksi_keagamaan" => $this->input->post('seksi_keagamaan'),
            "seksi_pemuda" => $this->input->post('seksi_pemuda'),
            "seksi_keagamaan" => $this->input->post('seksi_keagamaan'),
            "seksi_pemuda" => $this->input->post('seksi_pemuda'),
            "seksi_humas" => $this->input->post('seksi_humas'),
            "seksi_pembangunan_dan_kebersihan" => $this->input->post('seksi_pembangunan_dan_kebersihan'),
            "seksi_pemberdayaan_perempuan" => $this->input->post('seksi_pemberdayaan_perempuan'),
		);
		$this->db->insert('rw', $data);
	}

	public function edit($id)
	{
		$data = array(
			"nama_rw" => $this->input->post('nama_rw'),
			"nama_wilayah" => $this->input->post('nama_wilayah'),
			"ketua_rw" => $this->input->post('ketua_rw'),
			"penasihat" => $this->input->post('penasihat'),
			"sekretaris" => $this->input->post('sekretaris'),
			"bendahara" => $this->input->post('bendahara'),
			"seksi_keamanan" => $this->input->post('seksi_keamanan'),
            "seksi_keagamaan" => $this->input->post('seksi_keagamaan'),
            "seksi_pemuda" => $this->input->post('seksi_pemuda'),
            "seksi_keagamaan" => $this->input->post('seksi_keagamaan'),
            "seksi_pemuda" => $this->input->post('seksi_pemuda'),
            "seksi_humas" => $this->input->post('seksi_humas'),
            "seksi_pembangunan_dan_kebersihan" => $this->input->post('seksi_pembangunan_dan_kebersihan'),
            "seksi_pemberdayaan_perempuan" => $this->input->post('seksi_pemberdayaan_perempuan'),
		);
		
		$this->db->where('id', $id);
		$this->db->update('rw', $data);
	}

	public function delete($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('rw');
	}
}
