<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class SiswaModel extends CI_Model {
	
	public function view()
	{
		return $this->db->get('siswa')->result();
	}

	public function validation($mode)
	{
		$this->load->library('form_validation');
		if($mode == "save"){
			$this->form_validation->set_rules('input_nis', 'Nis', 'required|max_length[50]');
			$this->form_validation->set_rules('input_nama', 'Nama', 'required|max_length[50]');
			$this->form_validation->set_rules('input_jeniskelamin', 'Jenis Kelamin', 'required|max_length[1]');
			$this->form_validation->set_rules('input_telp', 'telp', 'required|max_length[13]');
			$this->form_validation->set_rules('input_alamat', 'Alamat', 'required');
		}elseif($mode == "update"){
			$this->form_validation->set_rules('input_nama', 'Nama', 'required|max_length[50]');
			$this->form_validation->set_rules('input_jeniskelamin', 'Jenis Kelamin', 'required|max_length[1]');
			$this->form_validation->set_rules('input_telp', 'telp', 'required|max_length[13]');
			$this->form_validation->set_rules('input_alamat', 'Alamat', 'required');
		}
		

		if($this->form_validation->run()){
			return true;
		}else{
			return false;
		}

		
	}

	public function save()
	{
		$data = array(
			"nis" => $this->input->post('input_nis'),
			"nama" => $this->input->post('input_nama'),
			"jenis_kelamin" => $this->input->post('input_jeniskelamin'),
			"telp" => $this->input->post('input_telp'),
			"alamat" => $this->input->post('input_alamat')
		);

		$this->db->insert('siswa', $data);
	}

	public function edit($id)
	{
		$data = array(
			"nis" => $this->input->post('input_nis'),
			"nama" => $this->input->post('input_nama'),
			"jenis_kelamin" => $this->input->post('input_jeniskelamin'),
			"telp" => $this->input->post('input_telp'),
			"alamat" => $this->input->post('input_alamat')
		);

		$this->db->where('id', $id);
		$this->db->update('siswa', $data);
	}

	public function delete($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('siswa');
	}

}
