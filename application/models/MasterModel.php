<?php 

if(!defined('BASEPATH')) exit('No direct script access allowed');

class MasterModel extends CI_Model {

	public function rw()
	{
		return $this->db->get('rw')->result();
	}

	public function rt()
	{
		return $this->db->get('rt')->result();
	}

	public function karangTaruna()
	{
		return $this->db->get('karang_taruna')->result();
	}

	public function posyandu()
	{
		return $this->db->get('posyandu')->result();
	}

	public function rab()
	{
		return $this->db->get('rab')->result();
	}

	public function desa()
	{
		return $this->db->get('data_desa')->result();
	}
}
