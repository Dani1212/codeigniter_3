<?php 

if(!defined('BASEPATH')) exit('No direct script access allowed');

class KarangTarunaModel extends CI_Model {
	public function view()
	{
		return $this->db->get('karang_taruna')->result();
	}

	public function show($id)
	{
		$this->db->select('*');
		$this->db->from('karang_taruna');
		$this->db->where('id', $id);

		return $this->db->get();
	}

	public function validation()
	{
		$this->load->library('form_validation');

		$this->form_validation->set_rules('nama_karang_taruna', 'Nama posyandu', 'required');
		$this->form_validation->set_rules('ketua', 'Ketua Posyandu', 'required');
		$this->form_validation->set_rules('wakil', 'Wakil Ketua', 'required');
		$this->form_validation->set_rules('sekretaris', 'Sekretaris', 'required');
		$this->form_validation->set_rules('bendahara', 'Bendahara', 'required');
	}

	public function save()
	{
		$data = array(
			"nama_karang_taruna" => $this->input->post('nama_karang_taruna'),
			"ketua" => $this->input->post('ketua'),
			"wakil" => $this->input->post('wakil'),
			"sekretaris" => $this->input->post('sekretaris'),
			"bendahara" => $this->input->post('bendahara'),
		);
		$this->db->insert('karang_taruna', $data);
	}

	public function edit($id)
	{
		$data = array(
			"nama_karang_taruna" => $this->input->post('nama_karang_taruna'),
			"ketua" => $this->input->post('ketua'),
			"wakil" => $this->input->post('wakil'),
			"sekretaris" => $this->input->post('sekretaris'),
			"bendahara" => $this->input->post('bendahara'),
		);
		
		$this->db->where('id', $id);
		$this->db->update('karang_taruna', $data);
	}

	public function delete($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('karang_taruna');
	}
}
