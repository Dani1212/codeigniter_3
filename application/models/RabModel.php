<?php 

if(!defined('BASEPATH')) exit('No direct script access allowed');

class RabModel extends CI_Model {
	public function view()
	{
		return $this->db->get('rab')->result();
	}

	public function show($id)
	{
		$this->db->select('*');
		$this->db->from('rab');
		$this->db->where('id', $id);

		return $this->db->get();
	}

	public function validation()
	{
		$this->load->library('form_validation');

		$this->form_validation->set_rules('uraian', 'Uraian', 'required');
		$this->form_validation->set_rules('tanggal', 'Tanggal', 'required');
		$this->form_validation->set_rules('anggaran', 'Anggaran', 'required');
		$this->form_validation->set_rules('perubahan', 'Perubahan', 'required');
		$this->form_validation->set_rules('jumlah', 'Jumlah', 'required');
		$this->form_validation->set_rules('jumlah_satuan', 'Jumlah Satuan', 'required');
		$this->form_validation->set_rules('harga_satuan', 'Harga Satuan', 'required');
		$this->form_validation->set_rules('sumber_dana', 'Sumber Dana', 'required');
	}

	public function save()
	{
		$data = array(
			"uraian" => $this->input->post('uraian'),
			"tanggal" => $this->input->post('tanggal'),
			"anggaran" => $this->input->post('anggaran'),
			"perubahan" => $this->input->post('perubahan'),
			"jumlah" => $this->input->post('jumlah'),
			"jumlah_satuan" => $this->input->post('jumlah_satuan'),
			"harga_satuan" => $this->input->post('harga_satuan'),
			"sumber_dana" => $this->input->post('sumber_dana'),
		);
		$this->db->insert('rab', $data);
	}

	public function edit($id)
	{
		$data = array(
			"uraian" => $this->input->post('uraian'),
			"tanggal" => $this->input->post('tanggal'),
			"anggaran" => $this->input->post('anggaran'),
			"perubahan" => $this->input->post('perubahan'),
			"jumlah" => $this->input->post('jumlah'),
			"jumlah_satuan" => $this->input->post('jumlah_satuan'),
			"harga_satuan" => $this->input->post('harga_satuan'),
			"sumber_dana" => $this->input->post('sumber_dana'),
		);
		
		$this->db->where('id', $id);
		$this->db->update('rab', $data);
	}

	public function delete($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('rab');
	}
}
