<div class="table-responsive mt-3">
  <table class="table table-bordered">
  	<thead class="thead-light">
	  	<tr>
			<th class="text-center">No</th>
			<th>Nama&nbsp;Jurusan</th>
			<th>Deskripsi&nbsp;Jurusan</th>
			<th colspan="2" class="text-center"><span class="fa fa-cog"></span> Aksi</th>
    	</tr>
	</thead>
    
    <?php
        $no = 1;
    		foreach($jurusan as $data){
    ?>
      <tr>
        <td class="align-middle text-center"><?php echo $no; ?></td>
        <td class="align-middle"><?php echo $data->nama_jurusan; ?></td>
        <td class="align-middle"><?php echo $data->deskripsi; ?></td>
        <td class="align-middle text-center">
          <a href="<?= base_url('jurusan/edit/'.$data->id) ?>" class="btn btn-success btn-sm"><span class="fa fa-edit"></span></a>                    
        </td>
        <td class="align-middle text-center">
          <a href="<?= base_url('jurusan/hapus/'. $data->id) ?>" onclick="return confirm('Yakin data dihapus?');"  class="btn btn-danger btn-sm "><span class="fa fa-trash"></span></a>
        </td>
      </tr>
    <?php
      $no++; // Tambah 1 setiap kali looping
    }
    ?>
  </table>
</div>


