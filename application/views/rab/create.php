<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <link href='https://fonts.googleapis.com/css?family=Poppins' rel='stylesheet'>
    <style>
        *{
            color: #505050;
        }

        body {
            font-family: 'Poppins';
            background: #f8f8f8;
        }

        .card{
            border-top-left-radius: 15px;
            border-top-right-radius: 15px;
            border-bottom-right-radius: 15px;
            border-bottom-left-radius: 15px;
        }
    </style>
    <title>RAB</title>
  </head>
  <body>

    <div class="container mt-5">
        <div class="row">
            <div class="col-md-12">
                <h4 class="font-weight-bold mb-4">Tambah Data</h4>

                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item"><a href="<?= base_url('rab') ?>">Data</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Input Data</li>
                    </ol>
                </nav>

                <?php if(isset($listErrors)) { ?>
                    <div class="alert alert-danger" role="alert">
                        <?= $listErrors ?>
                    </div>
                <?php } ?>
                <div class="card mb-5">
                    <div class="card-body">
                        <form action="<?php echo base_url('rab/simpan') ?>" method="POST">
							<div class="form-group">
								<label for="">Tanggal*</label>
								<input type="date" name="tanggal" id="" class="form-control">
							</div>
                            <div class="form-group">
                                <label>Uraian*</label>
                                <input type="text" class="form-control" name="uraian" autocomplete="off">
                            </div>
                            <div class="form-group">
                                <label>Anggaran*</label>
                                <input type="text" class="form-control" name="anggaran" autocomplete="off">
                            </div>
							<div class="form-group">
                                <label>Perubahan*</label>
                                <input type="text" class="form-control" name="perubahan" autocomplete="off">
                            </div>
							<div class="form-group">
                                <label>Jumlah*</label>
                                <input type="text" class="form-control" name="jumlah" autocomplete="off">
                            </div>
							<div class="form-group">
                                <label>Jumlah Satuan*</label>
                                <input type="text" class="form-control" name="jumlah_satuan" autocomplete="off">
                            </div>
							<div class="form-group">
                                <label>Harga Satuan*</label>
                                <input type="text" class="form-control" name="harga_satuan" autocomplete="off">
                            </div>
							<div class="form-group">
								<label for="">Sumber Dana*</label>
								<select name="sumber_dana" id="" class="form-control">
									<option value="">Pilih Sumber Dana</option>
									<option value="APB Desa">APB Desa</option>
									<option value="APBD">APBD</option>
									<option value="APBN">APBN</option>
								</select>
							</div>
                            <button type="submit" class="btn btn-primary">Simpan</button>
                            <button type="reset" class="btn btn-danger">Reset</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
  </body>
</html>
