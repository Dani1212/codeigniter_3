<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <link href='https://fonts.googleapis.com/css?family=Poppins' rel='stylesheet'>
    <style>
        *{
            color: #505050;
        }

        body {
            font-family: 'Poppins';
            background: #f8f8f8;
        }

        .card{
            border-top-left-radius: 15px;
            border-top-right-radius: 15px;
            border-bottom-right-radius: 15px;
            border-bottom-left-radius: 15px;
        }
    </style>
    <title>RT</title>
  </head>
  <body>

    <div class="container mt-5">
        <div class="row">
            <div class="col-md-12">
                <h4 class="font-weight-bold mb-4">Updata Data</h4>

                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item"><a href="<?= base_url('rt') ?>">Data</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Update Data</li>
                    </ol>
                </nav>

                <?php if(isset($listErrors)) { ?>
                    <div class="alert alert-danger" role="alert">
                        <?= $listErrors ?>
                    </div>
                <?php } ?>
                <div class="card mb-5">
                    <div class="card-body">
                        <form action="<?php echo base_url('rt/ubah/'.$rt->id) ?>" method="POST">
							<div class="form-group">
								<label for="">Nama RT*</label>
								<input type="text" name="nama_rt" value="<?= $rt->nama_rt ?>" id="" class="form-control">
							</div>
                            <div class="form-group">
								<label for="">Nama Wilayah*</label>
								<input type="text" name="nama_wilayah" value="<?= $rt->nama_wilayah ?>" id="" class="form-control">
							</div>
                            <div class="form-group">
                                <label>Ketua RT*</label>
                                <input type="text" class="form-control" name="ketua_rt" value="<?= $rt->ketua_rt ?>" autocomplete="off">
                            </div>
                            <div class="form-group">
                                <label>Penasihat*</label>
                                <input type="text" class="form-control" name="penasihat" autocomplete="off" value="<?= $rt->penasihat ?>">
                            </div>
							<div class="form-group">
                                <label>Sekretaris*</label>
                                <input type="text" class="form-control" name="sekretaris" autocomplete="off" value="<?= $rt->sekretaris ?>">
                            </div>
							<div class="form-group">
                                <label>Bendahara*</label>
                                <input type="text" class="form-control" name="bendahara" autocomplete="off" value="<?= $rt->bendahara ?>">
                            </div>
							<div class="form-group">
                                <label>Seksi Keamanan*</label>
                                <input type="text" class="form-control" name="seksi_keamanan" autocomplete="off" value="<?= $rt->seksi_keamanan ?>">
                            </div>
                            <div class="form-group">
                                <label>Seksi Keagamaan*</label>
                                <input type="text" class="form-control" name="seksi_keagamaan" autocomplete="off" value="<?= $rt->seksi_keagamaan ?>">
                            </div>
                            <div class="form-group">
                                <label>Seksi Pemuda*</label>
                                <input type="text" class="form-control" name="seksi_pemuda" autocomplete="off" value="<?= $rt->seksi_pemuda ?>">
                            </div>
                            <div class="form-group">
                                <label>Seksi Humas*</label>
                                <input type="text" class="form-control" name="seksi_humas" autocomplete="off" value="<?= $rt->seksi_humas ?>">
                            </div>
                            <div class="form-group">
                                <label>Seksi Pembangunan Dan Kebersihan*</label>
                                <input type="text" class="form-control" name="seksi_pembangunan_dan_kebersihan" autocomplete="off" value="<?= $rt->seksi_pembangunan_dan_kebersihan ?>">
                            </div>
                            <div class="form-group">
                                <label>Seksi Pemberdayaan Perempuan*</label>
                                <input type="text" class="form-control" name="seksi_pemberdayaan_perempuan" autocomplete="off" value="<?= $rt->seksi_pemberdayaan_perempuan ?>">
                            </div>
							
                            <button type="submit" class="btn btn-primary">Simpan</button>
                            <button type="reset" class="btn btn-danger">Reset</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
  </body>
</html>
