<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>RT</title>
  <!-- Load File bootstrap.min.css yang ada difolder css -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <link href='https://fonts.googleapis.com/css?family=Poppins' rel='stylesheet'>
  <script src="https://kit.fontawesome.com/a076d05399.js"></script>
  <style>
	body {
		font-family: 'Poppins';
	}
	.align-middle{
		vertical-align: middle !important;
	}
	button {
		font-size: 12px;
	}
	table{
		font-size: 14px;
	}
  </style>
    <script>
    var base_url = '<?= base_url() ?>' // Buat variabel base_url agar bisa di akses di semua file js
    </script>
</head>
<body>
	<div class="container mt-5">
		<div class="well">
			<a href="<?= base_url('rt/create') ?>" class="btn btn-primary pull-right">
				<span class="fa fa-plus"></span>  Tambah Data
			</a>
		</div>

		<div id="view" class="mt-2">
			<div class="table-responsive mt-3">
				<table class="table table-bordered">
					<thead class="thead-light">
						<tr>
							<th class="text-center">No</th>
							<th>Nama&nbsp;RT</th>
							<th>Nama&nbsp;Wilayah</th>
							<th>Ketua&nbsp;RT</th>
							<th>Penasihat</th>
							<th>Sekretaris</th>
							<th>Bendahara</th>
							<th>Seksi&nbsp;Keamanan</th>
                            <th>Seksi&nbsp;Keagamaan</th>
                            <th>Seksi&nbsp;Pemuda</th>
                            <th>Seksi&nbsp;Humas</th>
                            <th>Seksi&nbsp;Pembangunan&nbsp;Dan&nbsp;Kebersihan</th>
                            <th>Seksi&nbsp;Pemberdayaan&nbsp;Perempuan</th>
							<th colspan="2" class="text-center">Aksi</th>
						</tr>
					</thead>
					
					<?php
						$no = 1;
						foreach($rt as $data){
					?>
					<tr>
						<td class="align-middle text-center"><?php echo $no; ?></td>
						<td><?= $data->nama_rt ?></td>
						<td class="align-middle"><?php echo $data->nama_wilayah; ?></td>
						<td class="align-middle"><?php echo $data->ketua_rt; ?></td>
						<td class="align-middle"><?php echo $data->penasihat; ?></td>
						<td class="align-middle"><?php echo $data->sekretaris; ?></td>
						<td class="align-middle"><?php echo $data->bendahara; ?></td>
						<td class="align-middle"><?php echo $data->seksi_keamanan; ?></td>
                        <td class="align-middle"><?php echo $data->seksi_keagamaan; ?></td>
                        <td class="align-middle"><?php echo $data->seksi_pemuda; ?></td>
                        <td class="align-middle"><?php echo $data->seksi_humas; ?></td>
                        <td class="align-middle"><?php echo $data->seksi_pembangunan_dan_kebersihan; ?></td>
                        <td class="align-middle"><?php echo $data->seksi_pemberdayaan_perempuan; ?></td>
						<td class="align-middle text-center">
						<a href="<?= base_url('rt/edit/'.$data->id) ?>" class="btn btn-success btn-sm"><span class="fa fa-edit"></span></a>                    
						</td>
						<td class="align-middle text-center">
						<a href="<?= base_url('rt/hapus/'. $data->id) ?>" onclick="return confirm('Yakin data dihapus?');"  class="btn btn-danger btn-sm "><span class="fa fa-trash"></span></a>
						</td>
					</tr>
					<?php
						$no++; // Tambah 1 setiap kali looping
					}
					?>
				</table>
			</div>
		</div>
	</div>
	<!-- jQuery library -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<!-- Popper JS -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
	<!-- Latest compiled JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
	
</body>
</html>
