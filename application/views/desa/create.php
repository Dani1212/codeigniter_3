<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <link href='https://fonts.googleapis.com/css?family=Poppins' rel='stylesheet'>
    <style>
        *{
            color: #505050;
        }

        body {
            font-family: 'Poppins';
            background: #f8f8f8;
        }

        .card{
            border-top-left-radius: 15px;
            border-top-right-radius: 15px;
            border-bottom-right-radius: 15px;
            border-bottom-left-radius: 15px;
        }
    </style>
    <title>Tambah Data</title>
  </head>
  <body>

    <div class="container mt-5">
        <div class="row">
            <div class="col-md-12">
                <h4 class="font-weight-bold mb-4">Tambah Data</h4>

                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item"><a href="<?= base_url('desa') ?>">Data</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Input Data</li>
                    </ol>
                </nav>

                <?php if(isset($listErrors)) { ?>
                    <div class="alert alert-danger" role="alert">
                        <?= $listErrors ?>
                    </div>
                <?php } ?>
                <div class="card mb-5">
                    <div class="card-body">
                        <form action="<?php echo base_url('desa/simpan') ?>" method="POST">
                            <div class="form-group">
                                <label>Nama*</label>
                                <input type="text" class="form-control" name="nama" placeholder="Masukkan Nama" autocomplete="off">
                            </div>
                            <div class="form-group">
                                <label>Tempat Lahir*</label>
                                <input type="text" class="form-control" name="tempat_lahir" placeholder="Masukkan Tempat Lahir" autocomplete="off">
                            </div>
							<div class="form-group">
								<label for="">Tanggal Lahir*</label>
								<input type="date" name="tanggal_lahir" id="" class="form-control">
							</div>
							<div class="form-group">
								<label for="">Jenis Kelamin*</label>
								<select name="jenis_kelamin" id="" class="form-control">
									<option value="">Pilih Jenis Kelamin</option>
									<option value="Laki-laki">Laki-laki</option>
									<option value="Perempuan">Perempuan</option>
								</select>
							</div>
							<div class="form-group">
								<label for="">Alamat*</label>
								<textarea name="alamat" id="" rows="4" class="form-control"></textarea>
							</div>
							<div class="form-group">
								<label for="">Pendidikan Terakhir*</label>
								<select name="pendidikan_terakhir" id="" class="form-control">
									<option value="">Pilih Pendidikan Terakhir</option>
									<option value="SD">SD</option>
									<option value="SMP">SMP</option>
									<option value="SMA">SMA</option>
									<option value="D3">D3</option>
									<option value="S1">S1</option>
									<option value="S2">S2</option>
								</select>
							</div>
							<div class="form-group">
								<label for="">Jabatan*</label>
								<select name="jabatan" id="" class="form-control">
									<option value="">Pilih Jabatan</option>
									<option value="Kepala Desa">Kepala Desa</option>
									<option value="Wakil Kepala Desa">Wakil Kepala Desa</option>
									<option value="Sekretaris">Sekretaris</option>
									<option value="Bendahara">Bendahara</option>
									<option value="Pegawai">Pegawai</option>
									<option value="Lainnya">Lainnya</option>
								</select>
							</div>
							<div class="form-group">
								<label for="">Nomor SK</label>
								<input type="text" name="nomor_sk" placeholder="Masukkan Nomor SK" class="form-control" autocomplete="off">
							</div>
							<div class="form-group">
								<label for="">Tanggal SK</label>
								<input type="date" name="tanggal_sk" id="" class="form-control">
							</div>
							<div class="form-group">
								<label for="">Tanggal Pelantikan</label>
								<input type="date" name="tanggal_pelantikan" id="" class="form-control">
							</div>
							<div class="form-group">
								<label for="">Keterangan</label>
								<textarea name="keterangan" rows="4" class="form-control"></textarea>
							</div>
                            <button type="submit" class="btn btn-primary">Simpan</button>
                            <button type="reset" class="btn btn-danger">Reset</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
  </body>
</html>
