<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>CRUD Codeigniter 3</title>
  <!-- Load File bootstrap.min.css yang ada difolder css -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <link href='https://fonts.googleapis.com/css?family=Poppins' rel='stylesheet'>
  <script src="https://kit.fontawesome.com/a076d05399.js"></script>
  <style>
	body {
		font-family: 'Poppins';
	}
	.align-middle{
		vertical-align: middle !important;
	}
	button {
		font-size: 12px;
	}
	table{
		font-size: 14px;
	}
  </style>
    <script>
    var base_url = '<?= base_url() ?>' // Buat variabel base_url agar bisa di akses di semua file js
    </script>
</head>
<body>
	<div class="container mt-5">
		<div class="well">
			<a href="<?= base_url('desa/create') ?>" class="btn btn-primary pull-right">
				<span class="fa fa-plus"></span>  Tambah Data
			</a>
		</div>

		<div id="view" class="mt-2">
			<div class="table-responsive mt-3">
				<table class="table table-bordered">
					<thead class="thead-light">
						<tr>
							<th class="text-center">No</th>
							<th>Nama&nbsp;Pegawai</th>
							<th>Tempat&nbsp;Lahir</th>
							<th>Tanggal&nbsp;Lahir</th>
							<th>Jenis&nbsp;Kelamin</th>
							<th>Alamat</th>
							<th>Pendidikan&nbsp;Terakhir</th>
							<th>Jabatan</th>
							<th>Nomor&nbsp;SK</th>
							<th>Tanggal&nbsp;SK</th>
							<th>Tanggal&nbsp;Pelantikan</th>
							<th>Keterangan</th>
							<th colspan="2" class="text-center">Aksi</th>
						</tr>
					</thead>
					
					<?php
						$no = 1;
						foreach($desa as $data){
					?>
					<tr>
						<td class="align-middle text-center"><?php echo $no; ?></td>
						<td class="align-middle"><?php echo $data->nama; ?></td>
						<td class="align-middle"><?php echo $data->tempat_lahir; ?></td>
						<td class="align-middle"><?php echo $data->tanggal_lahir; ?></td>
						<td class="align-middle"><?php echo $data->jenis_kelamin; ?></td>
						<td class="align-middle"><?php echo $data->alamat; ?></td>
						<td class="align-middle"><?php echo $data->pendidikan_terakhir; ?></td>
						<td class="align-middle"><?php echo $data->jabatan; ?></td>
						<td class="align-middle"><?php echo $data->nomor_sk; ?></td>
						<td class="align-middle"><?php echo $data->tanggal_sk; ?></td>
						<td class="align-middle"><?php echo $data->tanggal_pelantikan; ?></td>
						<td class="align-middle"><?php echo $data->keterangan; ?></td>
						<td class="align-middle text-center">
						<a href="<?= base_url('desa/edit/'.$data->id) ?>" class="btn btn-success btn-sm"><span class="fa fa-edit"></span></a>                    
						</td>
						<td class="align-middle text-center">
						<a href="<?= base_url('desa/hapus/'. $data->id) ?>" onclick="return confirm('Yakin data dihapus?');"  class="btn btn-danger btn-sm "><span class="fa fa-trash"></span></a>
						</td>
					</tr>
					<?php
						$no++; // Tambah 1 setiap kali looping
					}
					?>
				</table>
			</div>
		</div>
	</div>
	<!-- jQuery library -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<!-- Popper JS -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
	<!-- Latest compiled JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
	
</body>
</html>
