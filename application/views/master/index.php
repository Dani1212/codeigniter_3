<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Master</title>
  <!-- Load File bootstrap.min.css yang ada difolder css -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <link href='https://fonts.googleapis.com/css?family=Poppins' rel='stylesheet'>
  <script src="https://kit.fontawesome.com/a076d05399.js"></script>
  <style>
	body {
		font-family: 'Poppins';
		background: #F8F8F8;
		color: #505050;
	}
	.align-middle{
		vertical-align: middle !important;
	}
	button {
		font-size: 12px;
	}
	table{
		font-size: 14px;
	}
	.paragraph{
		font-size: 18px;
	}
  </style>
    <script>
    var base_url = '<?= base_url() ?>' // Buat variabel base_url agar bisa di akses di semua file js
    </script>
</head>
<body>
	<div class="container mt-5">
		<h3>Desa Mekarsari</h3>
		<div class="row">
			<div class="col-md-4">
				<a href="<?= base_url('desa') ?>" class="text-decoration-none text-dark">
					<div class="card mt-3" style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);">
						<div class="card-body">
							<h5>Perangkat Desa</h5>
							<p class="paragraph"><?= count($desa) ?></p>
						</div>
					</div>
				</a>
			</div>
			<div class="col-md-4">
				<a href="<?= base_url('rw') ?>" class="text-decoration-none text-dark">
					<div class="card mt-3" style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);">
						<div class="card-body">
							<h5>Wilayah</h5>
							<p class="paragraph"><?= count($rw) ?></p>
						</div>
					</div>
				</a>
			</div>
			<div class="col-md-4">
				<a href="<?= base_url('rab') ?>" class="text-decoration-none text-dark">
					<div class="card mt-3" style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);">
						<div class="card-body">
							<h5>Rab Desa</h5>
							<p class="paragraph"><?= count($rab) ?></p>
						</div>
					</div>
				</a>
			</div>
		</div>
		
	</div>
	
	
	<!-- jQuery library -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<!-- Popper JS -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
	<!-- Latest compiled JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</body>
</html>
