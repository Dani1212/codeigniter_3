-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 17, 2020 at 07:58 AM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.2.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `crud_ci`
--

-- --------------------------------------------------------

--
-- Table structure for table `data_desa`
--

CREATE TABLE `data_desa` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `tempat_lahir` varchar(255) DEFAULT NULL,
  `tanggal_lahir` date DEFAULT NULL,
  `jenis_kelamin` varchar(255) DEFAULT NULL,
  `alamat` text DEFAULT NULL,
  `pendidikan_terakhir` varchar(255) DEFAULT NULL,
  `jabatan` varchar(255) DEFAULT NULL,
  `dasar_sk` varchar(255) DEFAULT NULL,
  `nomor_sk` varchar(255) DEFAULT NULL,
  `tanggal_sk` date DEFAULT NULL,
  `tanggal_pelantikan` date DEFAULT NULL,
  `keterangan` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `data_desa`
--

INSERT INTO `data_desa` (`id`, `nama`, `tempat_lahir`, `tanggal_lahir`, `jenis_kelamin`, `alamat`, `pendidikan_terakhir`, `jabatan`, `dasar_sk`, `nomor_sk`, `tanggal_sk`, `tanggal_pelantikan`, `keterangan`) VALUES
(1, 'Ali Mukti', 'Cianjur', '1982-08-08', 'Laki-laki', 'Kp. Ciberem', 'S1', 'Kepala Desa', NULL, '0238672438', '2018-08-08', '2015-08-08', '-');

-- --------------------------------------------------------

--
-- Table structure for table `jurusan`
--

CREATE TABLE `jurusan` (
  `id` int(11) NOT NULL,
  `nama_jurusan` varchar(255) DEFAULT NULL,
  `deskripsi` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `jurusan`
--

INSERT INTO `jurusan` (`id`, `nama_jurusan`, `deskripsi`) VALUES
(3, 'Teknik Mesin', 'ini merupakan jurusan teknik mesin'),
(4, 'Teknik Informatika', 'ini merupakan jurusan teknik Informatika'),
(5, 'Teknik Farmasi', 'ini merupakan jurusan teknik farmasi'),
(6, 'Teknik Management', 'ini merupakan jurusan teknik management');

-- --------------------------------------------------------

--
-- Table structure for table `karang_taruna`
--

CREATE TABLE `karang_taruna` (
  `id` int(11) NOT NULL,
  `nama_karang_taruna` varchar(255) DEFAULT NULL,
  `ketua` varchar(255) DEFAULT NULL,
  `wakil` varchar(255) DEFAULT NULL,
  `sekretaris` varchar(255) DEFAULT NULL,
  `bendahara` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `karang_taruna`
--

INSERT INTO `karang_taruna` (`id`, `nama_karang_taruna`, `ketua`, `wakil`, `sekretaris`, `bendahara`) VALUES
(1, 'Karang Taruna Pemuda', 'Gilang', 'Ucup', 'Kayla', 'Putri');

-- --------------------------------------------------------

--
-- Table structure for table `posyandu`
--

CREATE TABLE `posyandu` (
  `id` int(11) NOT NULL,
  `nama_posyandu` varchar(255) DEFAULT NULL,
  `ketua_posyandu` varchar(255) DEFAULT NULL,
  `wakil_posyandu` varchar(255) DEFAULT NULL,
  `sekretaris` varchar(255) DEFAULT NULL,
  `bendahara` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `posyandu`
--

INSERT INTO `posyandu` (`id`, `nama_posyandu`, `ketua_posyandu`, `wakil_posyandu`, `sekretaris`, `bendahara`) VALUES
(1, 'Posyandu Anggrek', 'Ida Rohida', 'Imas Fatimah', 'Ea Julaeha', 'Siti Rodiah');

-- --------------------------------------------------------

--
-- Table structure for table `rab`
--

CREATE TABLE `rab` (
  `id` int(11) NOT NULL,
  `uraian` varchar(255) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `anggaran` varchar(255) DEFAULT NULL,
  `perubahan` varchar(255) DEFAULT NULL,
  `jumlah` varchar(255) DEFAULT NULL,
  `jumlah_satuan` varchar(255) DEFAULT NULL,
  `harga_satuan` varchar(255) DEFAULT NULL,
  `sumber_dana` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `rab`
--

INSERT INTO `rab` (`id`, `uraian`, `tanggal`, `anggaran`, `perubahan`, `jumlah`, `jumlah_satuan`, `harga_satuan`, `sumber_dana`) VALUES
(1, 'Pembelian Semen ', '2020-11-03', '10000000', '0', '10', '1 sak', '1000000', 'APBD');

-- --------------------------------------------------------

--
-- Table structure for table `rt`
--

CREATE TABLE `rt` (
  `id` int(11) NOT NULL,
  `nama_rt` varchar(255) DEFAULT NULL,
  `nama_wilayah` varchar(255) DEFAULT NULL,
  `ketua_rt` varchar(255) DEFAULT NULL,
  `penasihat` varchar(255) DEFAULT NULL,
  `sekretaris` varchar(255) DEFAULT NULL,
  `bendahara` varchar(255) DEFAULT NULL,
  `seksi_keamanan` varchar(255) DEFAULT NULL,
  `seksi_keagamaan` varchar(255) DEFAULT NULL,
  `seksi_pemuda` varchar(255) DEFAULT NULL,
  `seksi_humas` varchar(255) DEFAULT NULL,
  `seksi_pembangunan_dan_kebersihan` varchar(255) DEFAULT NULL,
  `seksi_pemberdayaan_perempuan` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `rt`
--

INSERT INTO `rt` (`id`, `nama_rt`, `nama_wilayah`, `ketua_rt`, `penasihat`, `sekretaris`, `bendahara`, `seksi_keamanan`, `seksi_keagamaan`, `seksi_pemuda`, `seksi_humas`, `seksi_pembangunan_dan_kebersihan`, `seksi_pemberdayaan_perempuan`) VALUES
(1, '01', 'Jalan Bojongsari', 'Edi Cahyadi', 'Thomas Eden', 'Lila Fitria', 'Nazwa Suci', 'Komar Wahyudi', 'Kamaludin', 'Edin Junaedin', 'Yadi Pratama', 'Eman Sugeman', 'Rahmawati');

-- --------------------------------------------------------

--
-- Table structure for table `rw`
--

CREATE TABLE `rw` (
  `id` int(11) NOT NULL,
  `nama_rw` varchar(255) DEFAULT NULL,
  `nama_wilayah` varchar(255) DEFAULT NULL,
  `ketua_rw` varchar(255) DEFAULT NULL,
  `penasihat` varchar(255) DEFAULT NULL,
  `sekretaris` varchar(255) DEFAULT NULL,
  `bendahara` varchar(255) DEFAULT NULL,
  `seksi_keamanan` varchar(255) DEFAULT NULL,
  `seksi_keagamaan` varchar(255) DEFAULT NULL,
  `seksi_pemuda` varchar(255) DEFAULT NULL,
  `seksi_humas` varchar(255) DEFAULT NULL,
  `seksi_pembangunan_dan_kebersihan` varchar(255) DEFAULT NULL,
  `seksi_pemberdayaan_perempuan` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `rw`
--

INSERT INTO `rw` (`id`, `nama_rw`, `nama_wilayah`, `ketua_rw`, `penasihat`, `sekretaris`, `bendahara`, `seksi_keamanan`, `seksi_keagamaan`, `seksi_pemuda`, `seksi_humas`, `seksi_pembangunan_dan_kebersihan`, `seksi_pemberdayaan_perempuan`) VALUES
(2, '01', 'Kp. Ciberem', 'Fajar', 'Komarudin', 'Burhan', 'Beni', 'Bambang', 'Solihin', 'Japar', 'Wawan', 'Yusup', 'Soleh'),
(3, '02', 'Kp. Mekar', 'Ujang Hikmat', 'Dedi Hartadi', 'Lilis Sukmawati', 'Yanti Maryanti', 'Dadan Hidan', 'Heri Setiawan', 'Asep Peso', 'Tatang ', 'Adang', 'Fitri');

-- --------------------------------------------------------

--
-- Table structure for table `siswa`
--

CREATE TABLE `siswa` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `nis` varchar(11) DEFAULT NULL,
  `jenis_kelamin` varchar(1) DEFAULT NULL,
  `telp` varchar(13) DEFAULT NULL,
  `alamat` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `siswa`
--

INSERT INTO `siswa` (`id`, `nama`, `nis`, `jenis_kelamin`, `telp`, `alamat`) VALUES
(1, 'Ali', '0001', 'L', '087712341234', 'Kp. Caringin '),
(3, 'Bambang ', '0002', 'L', '087712341234', 'Kp. Caringin'),
(4, 'Cecep Hikmat', '0003', 'L', '087712341234', 'Kp. Caringin'),
(5, 'Dadang Hidayat', '0004', 'L', '087712341234', 'Kp. Waringin'),
(6, 'Firman ', '0005', 'L', '087712341234', 'Kp. Permataindah'),
(7, 'Hari Setiawan', '0006', 'L', '087712341234', 'Kp. Wangunsari');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `data_desa`
--
ALTER TABLE `data_desa`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jurusan`
--
ALTER TABLE `jurusan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `karang_taruna`
--
ALTER TABLE `karang_taruna`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `posyandu`
--
ALTER TABLE `posyandu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rab`
--
ALTER TABLE `rab`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rt`
--
ALTER TABLE `rt`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rw`
--
ALTER TABLE `rw`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `siswa`
--
ALTER TABLE `siswa`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `data_desa`
--
ALTER TABLE `data_desa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `jurusan`
--
ALTER TABLE `jurusan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `karang_taruna`
--
ALTER TABLE `karang_taruna`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `posyandu`
--
ALTER TABLE `posyandu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `rab`
--
ALTER TABLE `rab`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `rt`
--
ALTER TABLE `rt`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `rw`
--
ALTER TABLE `rw`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `siswa`
--
ALTER TABLE `siswa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
